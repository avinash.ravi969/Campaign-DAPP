pragma solidity ^0.4.0;

contract CampaignFactory {
    address[] public deployedCampaigns;
    
    function createCampaign(uint minimum) public {
        address newCampaign = new Campaign(minimum, msg.sender);
        deployedCampaigns.push(newCampaign);
    }
    
    function getDeployedCampaigns() public view returns (address[]) {
        return deployedCampaigns;
    }
}

contract Campaign {
   
    struct Request {
        string description;
        uint value;
        address recipient;
        bool complete;
        uint approvalCount;
        mapping(address => bool) approvals;
    }
    
    Request[] public requests;
    address public manager;
    uint public minimunFundingReq;
    mapping(address => bool) public approvers;
    uint public approversCount;
    
    function Campaign(uint minimum, address creator) public {
        manager = creator;
        minimunFundingReq = minimum;
    }
    
    function contribute() public payable {
        require(msg.value > minimunFundingReq);
        
        approvers[msg.sender] = true; 
        approversCount++;
    }
    
    function createRequest(string description, uint value, address recipient) public {
        require(msg.sender == manager);
        
        Request memory newRequest = Request({
            description : description,
            value : value,
            recipient : recipient,
            complete : false,
            approvalCount : 0
        });
        
        requests.push(newRequest);
    }
    
    function approveRequest(uint index) public {
        
        Request storage request = requests[index];
        
        require(approvers[msg.sender]);
        require(!request.approvals[msg.sender]);
        
        request.approvals[msg.sender] = true;
        request.approvalCount++;
    }
    
    function finalizeRequest(uint index) public {
        require(msg.sender == manager);
        require(requests[index].approvalCount > (approversCount/2));
        require(!requests[index].complete);
        
        requests[index].recipient.transfer(requests[index].value);
        requests[index].complete = true;
    }

    function getSummary() public view returns (uint, uint, uint, uint, address) {
        return ( minimunFundingReq, this.balance, requests.length, approversCount, manager );
    }

    function getRequestsCount() public view returns (uint) {
        return requests.length;
    }
}