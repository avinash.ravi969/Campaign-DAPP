const HdWalletProvider = require('truffle-hdwallet-provider');
const Web3 = require('web3');
const compiledFactory = require('./build/CampaignFactory.json');

const provider = new HdWalletProvider(
    'hundred bless ozone kangaroo post estate talent sleep urban word soup drum',
    'https://rinkeby.infura.io/v3/79808d7fab7544af92aa009e76f48151'
);

const web3 = new Web3(provider);

const deploy = async () => {
    const accounts = await web3.eth.getAccounts();
    console.log('Accounts ',accounts[0]);

    const result = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
            .deploy({ 
                data: '0x' + compiledFactory.bytecode
            })
            .send({ 
                from: accounts[0], 
                gas: '1000000'
            });
    console.log(JSON.parse(compiledFactory.interface));
    console.log("contract deployed to ", result.options.address);
};
deploy();
