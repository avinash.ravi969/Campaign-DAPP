import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from './../core/core.module';
import { CampaignCreateComponent } from './campaign-create/campaign-create.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignComponent } from './campaign.component';
import { CampaignRouting } from './campaign.routing';
import { CampaignViewComponent } from './campaign-view/campaign-view.component';
import { CampaignRequestsListComponent } from './campaign-requests-list/campaign-requests-list.component';

/**
 * Module to manage the application layout
 *
 * @export
 * @class LayoutModule
 */
@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    SharedModule,
    CampaignRouting,
  ],
  declarations: [
    CampaignComponent,
    CampaignCreateComponent,
    CampaignListComponent,
    CampaignViewComponent,
    CampaignRequestsListComponent
  ],
  exports: [
    CampaignCreateComponent,
    CampaignListComponent,
    CampaignViewComponent,
    CampaignRequestsListComponent
  ]
})
export class CampaignModule { }
