import { Web3Service } from './../../core/service/web3.service';
import { CampaignService } from './../../core/service/campaign.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-campaign-view',
  templateUrl: './campaign-view.component.html',
  styleUrls: ['./campaign-view.component.scss']
})
export class CampaignViewComponent implements OnInit {
  
  public campaign: any;
  public campaignAddress: any;
  public summary: any;
  public minimumFund: any;
  public balance: any;
  public requests: any;
  public approversCount: any;
  public manager: any;
  public accounts: any;
  public isContribute: boolean = false;
  
  public contributeFrm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private campaignService: CampaignService,
    private web3Service: Web3Service,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.campaignAddress = this.route.snapshot.params.id;
    console.log("Query Param  ", this.route.snapshot.params.id);

    this.campaign = this.campaignService.getCampaignInstance(this.route.snapshot.params.id);
    console.log("Camkhhuguguuu ",this.campaign);
    this.getCampaignSummary(this.campaign);

    this.contributeFrm = new FormGroup({
      'contibuteAmount' : new FormControl(null,Validators.required)
    });
  }

  async onContribute(){
    
    this.isContribute = true;
    this.accounts = await this.web3Service._web3.eth.getAccounts();

    try {
      await this.campaign.methods.contribute().send({ 
        from: this.accounts[0],
        value: this.web3Service._web3.utils.toWei(this.contributeFrm.value.contibuteAmount, 'ether')
      });
      this.isContribute = false;
      this.toastrService.success('SUCCESS', 'Contibuted Success');
    //  await this.getCampaignSummary(this.campaign);
      this.contributeFrm.reset();
    } catch(err){
      await this.toastrService.error('ERROR', err.message);
      this.isContribute = false;
    }
  }

  async getCampaignSummary(campaign: any) {
    this.summary = await campaign.methods.getSummary().call();
    console.log("summary Information ",this.summary);
    this.minimumFund = this.summary[0];
    this.balance = this.summary[1];
    this.requests = this.summary[2];
    this.approversCount = this.summary[3];
    this.manager = this.summary[4];
  }

}
