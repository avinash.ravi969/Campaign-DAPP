import { CreateCampaignModalComponent } from './../../shared/create-campaign-modal/create-campaign-modal.component';
import { Web3Service } from './../../core/service/web3.service';
import { CampaignFactoryService } from './../../core/service/campaign-factory.service';
import { Component, OnInit } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-campaign-list',
  templateUrl: './campaign-list.component.html',
  styleUrls: ['./campaign-list.component.scss']
})
export class CampaignListComponent implements OnInit {
  
  public campaigns: any;
  public accounts: [any];
  public errorMessage: any;
  public isCreate: boolean = false;

  constructor(
    private campaignFactoryService: CampaignFactoryService,
    private web3Service: Web3Service,
    private modalService : BsModalService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    this.getCampaignsDeployed();
  }
  
  async getCampaignsDeployed() {
    this.campaigns = await this.campaignFactoryService._istance.methods.getDeployedCampaigns().call();

    console.log(this.campaigns);
  }

  async createCampaign() {
    this.isCreate = true;
    this.accounts = await this.web3Service._web3.eth.getAccounts();
    const confirmModalRef = this.modalService.show(CreateCampaignModalComponent, { class: 'modal-md' });
    // Get the reference of the modal component that was loaded
    const confirmDialogComponent: CreateCampaignModalComponent = confirmModalRef.content;
    // Listen to event
    confirmDialogComponent.createdEvent.subscribe(async res => {
      console.log(res);
      console.log(this.accounts);
      try {
        await this.campaignFactoryService._istance.methods.createCampaign(res.minimumValue).send({ from: this.accounts[0]});
        this.isCreate = false;
        this.toastrService.success('SUCCESS', 'Campaign Created');
        this.getCampaignsDeployed();
      } catch(err){
        await this.toastrService.error('ERROR', err.message);
        this.isCreate = false;
      }
    });
  }
}
