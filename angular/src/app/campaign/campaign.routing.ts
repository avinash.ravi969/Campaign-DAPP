import { CampaignRequestsListComponent } from './campaign-requests-list/campaign-requests-list.component';
import { CampaignViewComponent } from './campaign-view/campaign-view.component';
import { CampaignCreateComponent } from './campaign-create/campaign-create.component';
import { CampaignListComponent } from './campaign-list/campaign-list.component';
import { CampaignComponent } from './campaign.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


/**
 * Manage all the routes in the application
 */
const appRoutes: Routes = [
    { path: '', component: CampaignComponent,
        children: [
        { path: '', component: CampaignListComponent },
        { path: 'campaign/create', component: CampaignCreateComponent },
        { path: 'campaign/:id', component: CampaignViewComponent },
        { path: 'campaign/requests/:id', component: CampaignRequestsListComponent },
    ]}
];

/**
 * Routing module
 *
 * @export
 * @class AppRoutingModule
 */
@NgModule({
    imports: [
        RouterModule.forChild(appRoutes)
    ],
    // Allow export of the configured RouterModule with the routes
    exports: [
        RouterModule
    ]
})
export class CampaignRouting {

}
