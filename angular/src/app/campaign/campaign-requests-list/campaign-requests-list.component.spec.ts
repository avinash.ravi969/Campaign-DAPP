import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignRequestsListComponent } from './campaign-requests-list.component';

describe('CampaignRequestsListComponent', () => {
  let component: CampaignRequestsListComponent;
  let fixture: ComponentFixture<CampaignRequestsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignRequestsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignRequestsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
