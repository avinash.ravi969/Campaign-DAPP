import { CampaignService } from './../../core/service/campaign.service';
import { ActivatedRoute } from '@angular/router';
import { Web3Service } from './../../core/service/web3.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CreateRequestModalComponent } from './../../shared/create-request-modal/create-request-modal.component';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-campaign-requests-list',
  templateUrl: './campaign-requests-list.component.html',
  styleUrls: ['./campaign-requests-list.component.scss']
})
export class CampaignRequestsListComponent implements OnInit {
  
  public accounts: any;
  public campaign: any;
  public isCreateRequest: boolean = false;
  public requestCounts: any;
  public requests: any;
  public approversCount: any;

  constructor(
    private modalService : BsModalService,
    private route: ActivatedRoute,
    public web3Service: Web3Service,
    private campaignService: CampaignService,
    private toastrService: ToastrService
  ) { }

  ngOnInit() {
    console.log("Requests");
    this.campaign = this.campaignService.getCampaignInstance(this.route.snapshot.params.id);

    this.getRequestCounts(); 
    this.getRequests();
    this.getApproversCount();
  }

  async getRequestCounts() {
    this.requestCounts = await this.campaign.methods.getRequestsCount().call();


    console.log("Requests Count ",this.requestCounts);
  }

  async getRequests() {
    this.requests = await Promise.all(
      Array(this.requests)
         .fill(this.requestCounts)
         .map((element, index) => {
           return this.campaign.methods.requests(index).call();
         })
    );

    console.log("Requests ",this.requests);
    console.log("Type Of request ", typeof this.requests);
  }
  
  async getApproversCount() {
    this.approversCount = await this.campaign.methods.approversCount().call();
  }

  async createRequest() {
    this.isCreateRequest = true;
    const confirmModalRef = this.modalService.show(CreateRequestModalComponent, { class: 'modal-md' });
    // Get the reference of the modal component that was loaded
    const confirmDialogComponent: CreateRequestModalComponent = confirmModalRef.content;
    // Listen to event
    confirmDialogComponent.createdEvent.subscribe(async res => {
      console.log("Request Response ",res);
      this.accounts = await this.web3Service._web3.eth.getAccounts();
      
      try {
        await this.campaign.methods.createRequest(res.description, this.web3Service._web3.utils.toWei(res.value, 'ether'), res.recipient).send({ from: this.accounts[0] });
        this.isCreateRequest = false;
        this.toastrService.success('SUCCESS', 'Request Creation Success');
      } catch (error) {
        await this.toastrService.error('ERROR', error.message);
        this.isCreateRequest = false;
      }
    });
  }

  async approveRequest(index: any) {
    this.accounts = await this.web3Service._web3.eth.getAccounts();
    await this.campaign.methods.approveRequest(index).send({ from: this.accounts[0] });
  }

  async finalizeRequest(index: any) {
    this.accounts = await this.web3Service._web3.eth.getAccounts();
    await this.campaign.methods.finalizeRequest(index).send({ from: this.accounts[0] });
  }

}
