import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CampaignCreationErrorComponent } from './campaign-creation-error.component';

describe('CampaignCreationErrorComponent', () => {
  let component: CampaignCreationErrorComponent;
  let fixture: ComponentFixture<CampaignCreationErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CampaignCreationErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CampaignCreationErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
