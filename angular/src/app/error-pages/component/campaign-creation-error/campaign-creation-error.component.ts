import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-campaign-creation-error',
  templateUrl: './campaign-creation-error.component.html',
  styleUrls: ['./campaign-creation-error.component.scss']
})
export class CampaignCreationErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
