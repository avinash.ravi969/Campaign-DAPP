import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { CampaignModule } from './campaign/campaign.module';
import { CoreModule } from './core/core.module';
import { AppRouting } from './app-routing';
import { ToastrModule } from 'ngx-toastr';
import { CampaignCreationErrorComponent } from './error-pages/component/campaign-creation-error/campaign-creation-error.component';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // ToastrModule added
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-top-right',
      // preventDuplicates: true,
      closeButton: true
    }),
    CoreModule,
    CampaignModule,
    AppRouting
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
