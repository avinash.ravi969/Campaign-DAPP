import { Web3Service } from './web3.service';
import { Injectable } from '@angular/core';

declare let require: any;
const compiledFactory = require('./../../../../../ethereum/build/CampaignFactory.json');

@Injectable()
export class CampaignFactoryService {
  
  public _istance: any;

  constructor(
    private web3Service: Web3Service
  ) { 
    this._istance = new this.web3Service._web3.eth.Contract(JSON.parse(compiledFactory.interface), '0x201b0f611DcFE665d6450CC51179A3E4839f8A88');
  }

}
