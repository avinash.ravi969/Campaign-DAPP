import { Web3Service } from './web3.service';
import { Injectable } from '@angular/core';

declare let require: any;
const campaign = require('./../../../../../ethereum/build/Campaign.json');

@Injectable()
export class CampaignService {

  constructor(
    private web3Service: Web3Service
  ) { }
  
  getCampaignInstance(address: any) {
    return new this.web3Service._web3.eth.Contract(JSON.parse(campaign.interface), address);
  }
}
