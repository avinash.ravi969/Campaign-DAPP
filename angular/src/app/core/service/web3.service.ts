import { Injectable } from '@angular/core';

const Web3 = require("web3");

declare let require: any;
declare let window: any;

@Injectable()
export class Web3Service {

  public _web3: any;
  
  constructor() { 
    if (typeof window.web3 !== 'undefined') {
      // Use Mist/MetaMask's provider
      this._web3 = new Web3(window.web3.currentProvider);
    }
  }

}
