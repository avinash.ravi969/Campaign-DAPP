import { TestBed, inject } from '@angular/core/testing';

import { CampaignFactoryService } from './campaign-factory.service';

describe('CampaignFactoryService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CampaignFactoryService]
    });
  });

  it('should be created', inject([CampaignFactoryService], (service: CampaignFactoryService) => {
    expect(service).toBeTruthy();
  }));
});
