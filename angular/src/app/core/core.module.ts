import { CampaignService } from './service/campaign.service';
import { CampaignFactoryService } from './service/campaign-factory.service';
import { Web3Service } from './service/web3.service';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    ModalModule.forRoot()
  ],
  declarations: [],
  exports: [
    FormsModule,
    RouterModule,
    ReactiveFormsModule,
    ModalModule
  ],
  providers: [
    Web3Service,
    CampaignFactoryService,
    CampaignService
  ]
})
export class CoreModule { }
