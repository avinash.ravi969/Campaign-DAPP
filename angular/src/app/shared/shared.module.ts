import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { CoreModule } from '../core/core.module';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { CreateCampaignModalComponent } from './create-campaign-modal/create-campaign-modal.component';
import { CreateRequestModalComponent } from './create-request-modal/create-request-modal.component';

/**
 * Module to manage the application layout
 *
 * @export
 * @class LayoutModule
 */
@NgModule({
  imports: [
    CommonModule,
    CoreModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    CreateCampaignModalComponent,
    CreateRequestModalComponent
  ],
  exports: [
    HeaderComponent,
    FooterComponent
  ],
  // All dynamic components are referenced as entry components
  entryComponents: [
    CreateCampaignModalComponent,
    CreateRequestModalComponent
]
})
export class SharedModule { }
