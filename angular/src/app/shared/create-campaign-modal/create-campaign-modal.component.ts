import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-create-campaign-modal',
  templateUrl: './create-campaign-modal.component.html',
  styleUrls: ['./create-campaign-modal.component.scss']
})
export class CreateCampaignModalComponent implements OnInit {

   // Event bindings
   @Output() createdEvent = new EventEmitter<any>();

   createCampaignFrm : FormGroup;

  constructor(
    public createCampaignModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.createCampaignFrm = new FormGroup({
      'minimumValue' : new FormControl(null,Validators.required)
    });
  }

  onCreateCampaign(){
    // Form the request data
    const reqData = {
      minimumValue: this.createCampaignFrm.value.minimumValue
    };

    console.log('onCreateCampaign', reqData);

    // Emit action
    this.createdEvent.emit(reqData);
    // Close modal
    this.createCampaignModalRef.hide();
  }

}
