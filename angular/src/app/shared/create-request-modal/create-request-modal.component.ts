import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-create-request-modal',
  templateUrl: './create-request-modal.component.html',
  styleUrls: ['./create-request-modal.component.scss']
})
export class CreateRequestModalComponent implements OnInit {

  // Event bindings
  @Output() createdEvent = new EventEmitter<any>();

  createRequestFrm : FormGroup;

  constructor(
    public createRequestModalRef: BsModalRef
  ) { }

  ngOnInit() {
    this.createRequestFrm = new FormGroup({
      'description' : new FormControl(null,Validators.required),
      'recipient' : new FormControl(null,Validators.required),
      'value' : new FormControl(null,Validators.required)
    });
  }

  onCreateRequest(){
    // Form the request data
    const reqData = {
      description: this.createRequestFrm.value.description,
      recipient: this.createRequestFrm.value.recipient,
      value: this.createRequestFrm.value.value
    };

    console.log('onCreateRequest', reqData);

    // Emit action
    this.createdEvent.emit(reqData);
    // Close modal
    this.createRequestModalRef.hide();
  }

}
