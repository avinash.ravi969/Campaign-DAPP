import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


/**
 * Manage all the routes in the application
 */
const appRoutes: Routes = [
];

/**
 * Routing module
 *
 * @export
 * @class AppRoutingModule
 */
@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    // Allow export of the configured RouterModule with the routes
    exports: [
        RouterModule
    ]
})
export class AppRouting {

}
